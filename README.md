# emulator_controller


Cut the controller shape from paper and make holes on each button position of the controller image.

<img src="/img/arcade_01.jpg" alt="01" width="650"/>

Cut strips of aluminum paper and place them behind each button. Secure them with tape.

<img src="/img/arcade_02.jpg" alt="02" width="650"/>

<img src="/img/arcade_03.jpg" alt="03" width="650"/>

Bend the top part of the aluminum strips onto the front side of the controller image.

<img src="/img/arcade_04.jpg" alt="04" width="650"/>

Tape the controller with the aluminum strips to a cardboard piece.

<img src="/img/arcade_05.jpg" alt="05" width="650"/>

Wire each button to the makey makey and make the appropriate changes in the emulator to succesfully control a game.